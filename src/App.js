import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import "./App.scss";
import LoginPage from "./Login/LoginPage";
import RegisterPage from "./Register/RegisterPage";
import Home from "./Home";
import Dashboard from "./Dashboard/Dashboard";

class App extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/login" component={LoginPage} />
          <Route path="/register" component={RegisterPage} />
          <Route path="/dashboard" component={Dashboard} />
        </Switch>
      </div>
    );
  }
}

export default App;
