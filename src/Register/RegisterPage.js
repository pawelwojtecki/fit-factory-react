import React, { Component } from "react";
import "./RegisterPage.scss";
import logo from "../logo.svg";
import {
  Grid,
  Row,
  Col,
  Button,
  FormGroup,
  FormControl,
  ControlLabel
} from "react-bootstrap";
import { Link } from "react-router-dom";

class RegisterPage extends Component {
  render() {
    return (
      <div className="login-page">
        <Grid>
          <Col lg={4} lgOffset={4} className="login-box">
            <Row>
              <Col>
                <form>
                  <FormGroup controlId="formBasicText">
                    <ControlLabel>Welcome to FIT FACTORY</ControlLabel>
                    <img src={logo} className="App-logo" alt="logo" />
                    <FormControl
                      type="text"
                      placeholder="Enter Username"
                      onChange={console.log("a")}
                    />
                    <FormControl.Feedback />
                    <FormControl
                      type="text"
                      placeholder="Enter Password"
                      onChange={console.log("a")}
                    />
                    <FormControl.Feedback />
                  </FormGroup>
                </form>
              </Col>
            </Row>
            <Row className="buttons">
              <Col lg={12}>
                <Button bsClass="login-btn my-button">LOGIN</Button>
              </Col>
            </Row>
            <Row className="login-box-footer">
              Already a member?
              <span className="create-account">
                <Link to="/login">Sign Up</Link>
              </span>
            </Row>
          </Col>
        </Grid>
      </div>
    );
  }
}

export default RegisterPage;
